<?php


use Digibase\Mecab\Mecab;
use PHPUnit\Framework\TestCase;

class MecabTest extends TestCase
{
    protected $mecab;

    protected function setUp(): void
    {
        $this->mecab = new Mecab();
    }

    public function test1()
    {
        $ret = $this->mecab->morphologicalAnalysis("すもももももももものうち");
        $this->assertEquals('すもも', $ret[0]->getSurface());
        $this->assertEquals('名詞', $ret[0]->getFeature()->getPos());
    }

    public function test2()
    {
        $ret = $this->mecab->separate("分かち書きを実行します。");
        $this->assertEquals('実行', $ret[2]);
    }

    public function test3()
    {
        $ret = $this->mecab->morphologicalAnalysisMultiple("もう寝る", 2);
        $this->assertCount(2, $ret);
        $this->assertEquals('寝', $ret[1]->getMecabResults()[1]->getSurface());
    }

    public function test4()
    {
        $ret = $this->mecab->morphologicalAnalysis("ビールを「ぷは～」と飲む");
        $this->assertTrue($ret[3]->getStat()->isUnknown());
    }

}
