<?php

namespace Digibase\Mecab;

use Digibase\Mecab\Entities\MecabResult;
use Digibase\Mecab\Entities\MecabResultArray;
use Digibase\Mecab\Entities\MecabResultFactory;
use Symfony\Component\Process\Process;

class Mecab
{
    const OPTION_DEFAULT = null;
    const OPTION_WAKATI = 'wakati';
    const OPTION_DUMP = 'dump';

    const DEFAULT_NULL_CHAR = '*';

    private MecabResultFactory $factory;

    /**
     *
     */
    public function __construct()
    {
        $this->factory = new MecabResultFactory();
    }

    /**
     * MorphologicalAnalysis
     * @param string $str
     * @return MecabResult[]
     */
    public function morphologicalAnalysis(string $str): array
    {
        $output = $this->exec($str, self::OPTION_DUMP);
        $ret = [];
        foreach (explode("\n", $output) as $l_idx => $line) {
            $param = explode(" ", $line);
            $morph_result = $this->factory->fromMecabOutput($param);
            if ($morph_result->getStat()->isBos() || $morph_result->getStat()->isEos())
                continue;
            $ret[] = $morph_result;
        }
        return $ret;
    }

    /**
     * @param string $str
     * @param int $n
     * @return MecabResultArray[]
     */
    public function morphologicalAnalysisMultiple(string $str, int $n): array
    {
        $output = $this->exec($str, self::OPTION_DUMP, $n);
        $ret = [];
        $buf = [];
        foreach (explode("\n", $output) as $l_idx => $line) {
            $param = explode(" ", $line);
            $morph_result = $this->factory->fromMecabOutput($param);
            if ($morph_result->getStat()->isBos()) {
                $buf = [];
            } else if ($morph_result->getStat()->isEos()) {
                $ret[] = new MecabResultArray($buf);
            } else {
                $buf[] = $morph_result;
            }
        }
        return $ret;
    }

    /**
     * Separate Words
     * @param string $str
     * @return array
     */
    public function separate(string $str): array
    {
        $output = $this->exec($str, self::OPTION_WAKATI);
        $ret = [];
        foreach (explode(' ', $output) as $row) {
            $ret[] = $row;
        }
        return $ret;
    }

    /**
     * Execute Mecab
     * @param string $str
     * @param string|null $option
     * @param int $n
     * @return string
     */
    public function exec(string $str, ?string $option = self::OPTION_DEFAULT, int $n = 1): string
    {
        $option_cmd = '';
        if ($option !== self::OPTION_DEFAULT) {
            $option_cmd = " -O {$option}";
        }
        $proc = Process::fromShellCommandline('echo "${:str}" | mecab -N"${:n}"' . $option_cmd);
        $proc->run(null, ['str' => $str, 'n' => $n]);
        return rtrim($proc->getOutput());
    }

}