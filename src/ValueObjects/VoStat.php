<?php

namespace Digibase\Mecab\ValueObjects;

class VoStat
{
    const STAT_DEFAULT = 0;
    const STAT_UNKNOWN = 1;
    const STAT_BOS = 2;
    const STAT_EOS = 3;

    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return static
     */
    public static function of(int $value): self
    {
        return new self($value);
    }

    public function isDefault(): bool
    {
        return $this->value === self::STAT_DEFAULT;
    }

    public function isUnknown(): bool
    {
        return $this->value === self::STAT_UNKNOWN;
    }

    public function isBos(): bool
    {
        return $this->value === self::STAT_BOS;
    }

    public function isEos(): bool
    {
        return $this->value === self::STAT_EOS;
    }


}