<?php

namespace Digibase\Mecab\ValueObjects;

class VoPosId
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return static
     */
    public static function of(int $value): self
    {
        return new self($value);
    }

    public function isNoun(): bool
    {
        return ($this->value >= 36 && $this->value <= 67);
    }

    public function isProperNoun(): bool
    {
        return ($this->value >= 41 && $this->value <= 47);
    }

    public function isVerb(): bool
    {
        return ($this->value >= 31 && $this->value <= 33);
    }
}