<?php

namespace Digibase\Mecab\Entities;

class MecabResultArray
{
    /**
     * @var MecabResult[]
     */
    private array $mecab_results;

    public function __construct(array $mecab_results)
    {
        $this->mecab_results = $mecab_results;
    }

    /**
     * @return MecabResult[]
     */
    public function getMecabResults(): array
    {
        return $this->mecab_results;
    }

}