<?php

namespace Digibase\Mecab\Entities;

use Digibase\Mecab\ValueObjects\VoPosId;
use Digibase\Mecab\ValueObjects\VoStat;

class MecabResultFactory
{
    /**
     * @param array $params
     * @return MecabResult
     */
    public function fromMecabOutput(array $params): MecabResult
    {
        $stat = VoStat::of($params[9]);
        $feature = explode(',', $params[2]);
        $not_bos_eos = ($stat->isDefault() || $stat->isUnknown());

        $mecab_feature = $not_bos_eos ?
            new MecabFeature(
                $feature[0],
                [$feature[1], $feature[2], $feature[3]],
                $feature[4],
                $feature[5],
                $feature[6],
                $feature[7] ?? null,
                $feature[8] ?? null
            ) : null;

        $info = $not_bos_eos ?
            new MecabInfo(
                $params[0],
                $params[3],
                $params[4],
                $params[5],
                $params[6],
                $params[8],
                $params[10],
                $params[11],
                $params[12],
                $params[13],
                $params[14],
                $params[15] ?? null,
            ) : null;

        $pos_id = $not_bos_eos ? VoPosId::of($params[7]) : null;

        return new MecabResult(
            $stat,
            $params[1],
            $mecab_feature,
            $pos_id,
            $info
        );
    }
}