<?php

namespace Digibase\Mecab\Entities;

class MecabFeature
{
    private string $pos;
    private array $pos_details = [];
    private string $inflection;
    private string $conjugated;
    private string $original;
    private ?string $reading;
    private ?string $pronunciation;

    /**
     * @param string $pos
     * @param array $pos_details
     * @param string $inflection
     * @param string $conjugated
     * @param string $original
     * @param string|null $reading
     * @param string|null $pronunciation
     */
    public function __construct(
        string  $pos,
        array   $pos_details,
        string  $inflection,
        string  $conjugated,
        string  $original,
        ?string $reading,
        ?string $pronunciation
    )
    {
        $this->pos = $pos;
        $this->pos_details = $pos_details;
        $this->inflection = $inflection;
        $this->conjugated = $conjugated;
        $this->original = $original;
        $this->reading = $reading;
        $this->pronunciation = $pronunciation;
    }


    /**
     * 品詞
     * @return string
     */
    public function getPos(): string
    {
        return $this->pos;
    }

    /**
     * 品詞細分類
     * @return array
     */
    public function getPosDetails(): array
    {
        return $this->pos_details;
    }

    /**
     * 活用型
     * @return string
     */
    public function getInflection(): string
    {
        return $this->inflection;
    }

    /**
     * 活用形
     * @return string
     */
    public function getConjugated(): string
    {
        return $this->conjugated;
    }

    /**
     * 原形
     * @return string
     */
    public function getOriginal(): string
    {
        return $this->original;
    }

    /**
     * 読み
     * @return string|null
     */
    public function getReading(): ?string
    {
        return $this->reading;
    }

    /**
     * 発音
     * @return string|null
     */
    public function getPronunciation(): ?string
    {
        return $this->pronunciation;
    }
}