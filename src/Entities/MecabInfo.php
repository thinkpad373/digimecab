<?php

namespace Digibase\Mecab\Entities;

class MecabInfo
{
    private int $node_id;
    private int $start_edge;
    private int $end_edge;
    private int $rc_attr;
    private int $lc_attr;
    private int $char_type;
    private bool $is_best;
    private int $alpha;
    private int $beta;
    private int $prob;
    private int $cost;
    private ?int $left_node_id;

    /**
     * @param int $node_id
     * @param int $start_edge
     * @param int $end_edge
     * @param int $rc_attr
     * @param int $lc_attr
     * @param int $char_type
     * @param bool $is_best
     * @param int $alpha
     * @param int $beta
     * @param int $prob
     * @param int $cost
     * @param int|null $left_node_id
     */
    public function __construct(
        int  $node_id,
        int  $start_edge,
        int  $end_edge,
        int  $rc_attr,
        int  $lc_attr,
        int  $char_type,
        bool $is_best,
        int  $alpha,
        int  $beta,
        int  $prob,
        int  $cost,
        ?int $left_node_id
    )
    {
        $this->node_id = $node_id;
        $this->start_edge = $start_edge;
        $this->end_edge = $end_edge;
        $this->rc_attr = $rc_attr;
        $this->lc_attr = $lc_attr;
        $this->char_type = $char_type;
        $this->is_best = $is_best;
        $this->alpha = $alpha;
        $this->beta = $beta;
        $this->prob = $prob;
        $this->cost = $cost;
        $this->left_node_id = $left_node_id;
    }

    /**
     * @return int
     */
    public function getNodeId(): int
    {
        return $this->node_id;
    }

    /**
     * @return int
     */
    public function getStartEdge(): int
    {
        return $this->start_edge;
    }

    /**
     * @return int
     */
    public function getEndEdge(): int
    {
        return $this->end_edge;
    }

    /**
     * @return int
     */
    public function getRcAttr(): int
    {
        return $this->rc_attr;
    }

    /**
     * @return int
     */
    public function getLcAttr(): int
    {
        return $this->lc_attr;
    }

    /**
     * @return int
     */
    public function getCharType(): int
    {
        return $this->char_type;
    }

    /**
     * @return bool
     */
    public function isIsBest(): bool
    {
        return $this->is_best;
    }

    /**
     * @return int
     */
    public function getAlpha(): int
    {
        return $this->alpha;
    }

    /**
     * @return int
     */
    public function getBeta(): int
    {
        return $this->beta;
    }

    /**
     * @return int
     */
    public function getProb(): int
    {
        return $this->prob;
    }

    /**
     * @return int
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @return int|null
     */
    public function getLeftNodeId(): ?int
    {
        return $this->left_node_id;
    }


}