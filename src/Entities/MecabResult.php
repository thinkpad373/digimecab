<?php

namespace Digibase\Mecab\Entities;

use Digibase\Mecab\ValueObjects\VoPosId;
use Digibase\Mecab\ValueObjects\VoStat;

class MecabResult
{
    private VoStat $stat;
    private string $surface;
    private ?MecabFeature $feature;
    private ?VoPosId $pos_id;
    private ?MecabInfo $info;

    /**
     * @param VoStat $stat
     * @param string $surface
     * @param MecabFeature|null $feature
     * @param VoPosId|null $pos_id
     * @param MecabInfo|null $info
     */
    public function __construct(
        VoStat        $stat,
        string        $surface,
        ?MecabFeature $feature,
        ?VoPosId      $pos_id,
        ?MecabInfo    $info
    )
    {
        $this->stat = $stat;
        $this->surface = $surface;
        $this->feature = $feature;
        $this->pos_id = $pos_id;
        $this->info = $info;
    }

    /**
     * @return VoStat
     */
    public function getStat(): VoStat
    {
        return $this->stat;
    }

    /**
     * @return string
     */
    public function getSurface(): string
    {
        return $this->surface;
    }

    /**
     * @return MecabFeature|null
     */
    public function getFeature(): ?MecabFeature
    {
        return $this->feature;
    }

    /**
     * @return VoPosId|null
     */
    public function getPosId(): ?VoPosId
    {
        return $this->pos_id;
    }

    /**
     * @return MecabInfo|null
     */
    public function getInfo(): ?MecabInfo
    {
        return $this->info;
    }

}